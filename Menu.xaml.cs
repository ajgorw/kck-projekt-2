﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KCK_Projekt_2
{
    /// <summary>
    /// Logika interakcji dla klasy Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        int accountBallance = 50;
        public Menu(int accountBallance = 0)
        {
            this.accountBallance += accountBallance;
            InitializeComponent();
        }

        //Zamknięcie
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        //start gry
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            StakeWindow window = new StakeWindow(accountBallance);
            this.Close();
            window.Show();
            
        }
        //Pokazanie stanu konta
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string balance = Convert.ToString(accountBallance);
            MessageBox.Show("Stan Twojego konta wynosi " + balance, "Stan Konta");
        }
        //Zwiekszenie stanu konta
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            AddBalance window = new AddBalance();
            this.Close();
            window.Show();
        }
    }
}
