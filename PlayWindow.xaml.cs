﻿using KCK_Projekt_2.Logic;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KCK_Projekt_2
{
    /// <summary>
    /// Logika interakcji dla klasy PlayWindow.xaml
    /// </summary>
    public partial class PlayWindow : Window
    {
        int stake;
        int nextCroupierCardImageViewIndex;
        int nextPlayerCardImageViewIndex;
        static List<Karta> krupier = new List<Karta>();
        static List<Karta> gracz = new List<Karta>();
        static int sumagracza;
        static int sumakrupiera;
        int accountBalance;

        public PlayWindow(int accountBalance, int stake)
        {
            InitializeComponent();

            sumagracza = 0;
            sumakrupiera = 0;
            //Do zaimplementowania stankonta wczytywany z poprzedniego menu
            this.accountBalance = accountBalance;
            this.stake = stake;
            Casino.tasuj(krupier,gracz);
            nextCroupierCardImageViewIndex = 0;
            nextPlayerCardImageViewIndex = 0;
            firstDeal();


        }




        //Przycisk odpowiadajacy za dobranie karty dla gracza oraz wyswietlenie jej
        private void Button_Click(object sender, RoutedEventArgs e)
        {
           

            Casino.dobierzkarte(gracz);
            sumagracza += gracz.Last().wartosc;
          
            //Wyswietlenie w zaleznosci od wartosci zmiennej
            setPlayerNextCardImage(Casino.zwrocObrazKarty(gracz.Last(),false));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Croupier_Image1.Source = Casino.zwrocObrazKarty(krupier[0], false);
            bool  infromationWasShowed = false;



            if (sumagracza > 21 && infromationWasShowed == false)
            {
               
               
                MessageBox.Show("Suma twoich oczek przekroczyla 21", "Przegrałeś :(");
                infromationWasShowed = true;
                accountBalance -= stake;
                //System.Threading.Thread.Sleep(3000);
                //menu();
                InitializeComponent();
                Menu window = new Menu(-stake);
                this.Close();
                window.Show();
            }
            while (sumakrupiera < 17)
            {
             
                Casino.dobierzkarte(krupier);
                sumakrupiera += krupier.Last().wartosc;
                setCroupierNextCardImage(Casino.zwrocObrazKarty(krupier.Last(), false));
                
            }
            if (sumakrupiera > 21 && infromationWasShowed == false)
            {
                infromationWasShowed = true;

                MessageBox.Show(" SUMA OCZEK KRUPIERA PRZEKROCZYLA 21", "Wygrałeś! :)");
                accountBalance += stake;
                InitializeComponent();
                Menu window = new Menu(stake);
                this.Close();
                window.Show();

                //menu();
            }
            if (sumagracza > sumakrupiera && infromationWasShowed == false)
            {
                infromationWasShowed = true;

                MessageBox.Show("SUMA TWOICH OCZEK JEST BLIZSZA 21", "Wygrałeś! :)");
                accountBalance += stake;
                InitializeComponent();
                Menu window = new Menu(stake);
                this.Close();
                window.Show();

                //menu();
            }
            else if (sumakrupiera > sumagracza && infromationWasShowed == false)
            {
                infromationWasShowed = true;

                MessageBox.Show("SUMA OCZEK KRUPIERA JEST BLIZSZA 21", "Przegrałeś :(");
                accountBalance -= stake;
                //menu();
                InitializeComponent();
                Menu window = new Menu(-stake);
                this.Close();
                window.Show();

            }
            else if ( infromationWasShowed == false)
            {
                infromationWasShowed = true;

                MessageBox.Show(" SUMY WASZYCH OCZEK SA IDENTYCZNE", "Remis :]");
                //menu();
                InitializeComponent();
                Menu window = new Menu();
                this.Close();
                window.Show();

            }
        }
        public void setCroupierNextCardImage(BitmapImage image)
        {
            nextCroupierCardImageViewIndex++;
            if (nextCroupierCardImageViewIndex == 1)
                Croupier_Image1.Source = image;
            if (nextCroupierCardImageViewIndex == 2)
                Croupier_Image2.Source = image;
            if (nextCroupierCardImageViewIndex == 3)
                Croupier_Image3.Source = image;
            if (nextCroupierCardImageViewIndex == 4)
                Croupier_Image4.Source = image;
            if (nextCroupierCardImageViewIndex == 5)
                Croupier_Image5.Source = image;
            if (nextCroupierCardImageViewIndex == 6)
                Croupier_Image6.Source = image;
            if (nextCroupierCardImageViewIndex == 7)
                Croupier_Image7.Source = image;
            if (nextCroupierCardImageViewIndex == 8)
                Croupier_Image8.Source = image;
        }
        public void setPlayerNextCardImage(BitmapImage image)
        {
            nextPlayerCardImageViewIndex++;
            if (nextPlayerCardImageViewIndex == 1)
                Player_Image1.Source = image;
            else if (nextPlayerCardImageViewIndex == 2)
                Player_Image2.Source = image;
            else if (nextPlayerCardImageViewIndex == 3)
                Player_Image3.Source = image;
            else if (nextPlayerCardImageViewIndex == 4)
                Player_Image4.Source = image;
            else if (nextPlayerCardImageViewIndex == 5)
                Player_Image5.Source = image;
            else if (nextPlayerCardImageViewIndex == 6)
                Player_Image6.Source = image;
            else if (nextPlayerCardImageViewIndex == 7)
                Player_Image7.Source = image;
            else if (nextPlayerCardImageViewIndex == 8)
                Player_Image8.Source = image;
        }
        public void firstDeal()
        {
            krupier.Add(Casino.losujkarte());
            sumakrupiera += krupier.Last().wartosc;
            krupier.Add(Casino.losujkarte());
            sumakrupiera += krupier.Last().wartosc;
            setCroupierNextCardImage(Casino.zwrocObrazKarty(krupier[0],true));
            setCroupierNextCardImage(Casino.zwrocObrazKarty(krupier[1],false));
            gracz.Add(Casino.losujkarte());
            sumagracza += gracz.Last().wartosc;
            gracz.Add(Casino.losujkarte());
            sumagracza += gracz.Last().wartosc;
            setPlayerNextCardImage(Casino.zwrocObrazKarty(gracz[0],false));
            setPlayerNextCardImage(Casino.zwrocObrazKarty(gracz[1],false));
        }
    }
}
