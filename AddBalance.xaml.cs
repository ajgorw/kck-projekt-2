﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KCK_Projekt_2
{
    /// <summary>
    /// Logika interakcji dla klasy AddBalance.xaml
    /// </summary>
    public partial class AddBalance : Window
    {
        public AddBalance()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int balance = Convert.ToInt32(textBox.Text);
            Menu window = new Menu(balance);
            this.Close();
            window.Show();
        }
    }
}
