﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace KCK_Projekt_2.Logic
{
    static class Casino
    {
        static Karta[] talia = new Karta[52];
        //Trefl = Clubs = C
        //Pik = Spades = S
        //Kier = Hearts = H
        //Karo = Diamonds = D
        static String[] kolor = new String[4] { "H", "S", "D", "C" };

        public static void tasuj(List<Karta> krupier, List<Karta> gracz)
        {
            krupier.Clear();
            gracz.Clear();
         

            int indeks = 0;
            int indeks2 = 0;
            for (int i = 8; i < 44; i++)
            {
                talia[indeks] = new Karta(i / 4, true, (i / 4).ToString(), kolor[indeks2]);
                indeks++;
                indeks2 = (indeks2 + 1) % 4;

            }
            for (int i = 0; i < 4; i++)
            {
                talia[indeks] = new Karta(10, true, "J", kolor[indeks2]);
                indeks++;
                indeks2 = (indeks2 + 1) % 4;

            }
            for (int i = 0; i < 4; i++)
            {
                talia[indeks] = new Karta(10, true, "Q", kolor[indeks2]);
                indeks++;
                indeks2 = (indeks2 + 1) % 4;

            }
            for (int i = 0; i < 4; i++)
            {
                talia[indeks] = new Karta(10, true, "K", kolor[indeks2]);
                indeks++;
                indeks2 = (indeks2 + 1) % 4;

            }
            for (int i = 0; i < 4; i++)
            {
                talia[indeks] = new Karta(11, true, "A", kolor[indeks2]);
                indeks++;
                indeks2 = (indeks2 + 1) % 4;

            }


        }
        public static Karta losujkarte()
        {
            Random rand = new Random();
            while (true)
            {
                int los = rand.Next(52);

                if (talia[los].dostepna == true)
                {
                    talia[los].dostepna = false;
                    return talia[los];
                }
            }
        }
        public static void dobierzkarte(List<Karta> lista)
        {
            //W tej metodzie trzeba zaimplementowac wczytywanie obrazka do pamięci
            if (lista.Count < 8)
                lista.Add(losujkarte());

            Karta k = losujkarte();

        }
        public static BitmapImage zwrocObrazKarty(Karta karta, Boolean z)
        {
            if (z == false)
            {
                //Tutaj jezeli ma byc karta pokazana
                string source = "\\Cards\\" + karta.nazwa + "\\" + karta.nazwa + karta.kolor + ".png";
                BitmapImage b = new BitmapImage();
                b.BeginInit();
                b.UriSource = new Uri(source, UriKind.Relative);

                b.EndInit();
                return b;

            }
            else
            {
                //Tutaj jezeli nie
                string source = "\\Cards\\" + "back.png";
                BitmapImage b = new BitmapImage();
                b.BeginInit();
                b.UriSource = new Uri(source, UriKind.Relative);

                b.EndInit();
                return b;

            }
        }

    }
}
